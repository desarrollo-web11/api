<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index() { // GET
        $product = Product::all();
        return $product;
    }

/*    public function create() { // GET
        //
    }*/

    public function store(Request $request) {
        Product::create([
            'name' => $request->name,
            'amount' => $request->amount,
            'price' => $request->price
        ]);

        return Product::all();
    }

/*    public function show(Product $product) {
        //
    }*/

/*    public function edit(Product $product) {
        //
    }*/

    public function update(Request $request) {
        $product = Product::find($request->id);
        $product->update([
            'name' => $request->name,
            'amount' => $request->amount,
            'price' => $request->price
        ]);

        return Product::all();
    }

    public function destroy(Request $request) {
        $product = Product::find($request->id);
        $product->delete();

        return Product::all();
    }
}

