<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('amount');
            $table->float('price');
            $table->timestamps();
        });

        DB::table('products')->insert([
            'name' => 'producto1',
            'amount' => 2,
            'price' => 20.50
        ]);

        DB::table('products')->insert([
            'name' => 'producto2',
            'amount' => 4,
            'price' => 100.50
        ]);

        DB::table('products')->insert([
            'name' => 'producto3',
            'amount' => 10,
            'price' => 11
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
};
